package lambda;

@FunctionalInterface
interface Operation{
	int opt(int a, int b);
	//public int opt(int x, int y);
}
public class Main1 {
	public static void result(int x, int y, Operation obj) {
		System.out.println(obj.opt(x, y));
	}
public static void main(String[] args) {
	result(50, 50, (a,b) -> (a+b));
	result(50, 50, (a,b) -> (a-b));
	result(50, 50, (a,b) -> (a*b));
	result(50, 50, (a,b) -> (a/b));
	
}
}
