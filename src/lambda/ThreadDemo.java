package lambda;

public class ThreadDemo {
		
    	public static void main(String []args) {
    		
    		Runnable r = () -> {
    			for (int i = 1; i <= 10; i++) {
    				System.out.println("value of i " + i);
    				try {
    					Thread.sleep(1000);
    				} catch (InterruptedException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    			}

    		};
    		
    		Runnable r2 =()->{
    			for(int i=1;i<=10;i++) {
    				System.out.println(i*2);
    				try {
    					Thread.sleep(2000);
    				} catch (InterruptedException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    			}
    		};

    		Thread t1 = new Thread(r);
    		t1.setName("thread1");
    		t1.start();
    		
    		Thread t2 = new Thread(r2);
    		t2.setName("thread2");
    		t2.start();
    	}
}
