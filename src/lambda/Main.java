package lambda;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     MyInter myInter = new MyInterImpl();
     myInter.hello();                     // 1 way of doing  
     
    MyInter myInter1=new MyInter() {

		@Override
		public void hello() {
			System.out.println("Hello World anonymously");		
		}
    	                                   // 2nd way of doing
    };
    myInter1.hello();
    
    MyInter inter2=()->System.out.println("Hello World using lambda");
    inter2.hello();
    
    SumInter sumInter = (a,b)-> a+b;
    System.out.println(sumInter.sum(10, 20));
    
    LengthInter lengthInter = (str)->str.length();
    System.out.println(lengthInter.length("Hello!"));
	}

}
