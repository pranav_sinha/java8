package Java8;

interface DoubleColon{
	void show();
}
public class MethodRef {
	public static void display() {
		System.out.println("its showing");
	}
	public void dis() {
		System.out.println("its showing");
	}
	public static void main(String [] args) {
 //DoubleColon db = (MethodRef :: display);
	DoubleColon db = new MethodRef()::dis;
 db.show();
}
}
