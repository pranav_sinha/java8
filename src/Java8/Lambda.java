package Java8;

@FunctionalInterface
interface Anonymous {
	//void show();
	void show(int n, int m);
	//void help();
	static void hello() {
		
	}
	//void add(int a, int b);
}

/*
 * //Before understanding lambda's we need to understatnd what are anonymous
 * f(x) public class Lambda implements Anonymous{
 * 
 * @Override public void show() { // I Way System.out.println("its showing");
 * 
 * }
 * 
 * public static void main(String[] args) { Lambda l = new Lambda(); l.show(); }
 * }
 * 
 * interface Anonymous{ void show(); }
 */

/*
 * //Before understanding lambda's we need to understatnd what are anonymous
 * f(x) public class Lambda { //create annonymous class public static void
 * main(String[] args) { Anonymous obj = new Anonymous() {
 * 
 * @Override public void show() { System.out.println("its showing");
 * 
 * }
 * 
 * @Override public void add(int a, int b) { System.out.println(a+b);
 * 
 * } };
 * 
 * obj.show(); obj.add(5,6); } }
 */


//Before understanding lambda's we need to understatnd what are anonymous f(x)
public class Lambda {
//create annonymous class
	public static void main(String[] args) {
		Anonymous obj = (x,y) -> {System.out.println(x+y);
		};                                                      //Why - to increase readability	               // only work with Functional Interfaces
		obj.show(5,6);
	}
}
