package Streamss;

public class Student {
String name;
String subject;
int marks;
public Student(String name, String subject, int marks) {
	super();
	this.name = name;
	this.subject = subject;
	this.marks = marks;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getSubject() {
	return subject;
}
public void setSubject(String subject) {
	this.subject = subject;
}
public int getMarks() {
	return marks;
}
public void setMarks(int marks) {
	this.marks = marks;
}
}
