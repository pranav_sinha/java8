package Streamss;

import java.util.Arrays;
import java.util.List;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamsApi {
public static void main(String[] args) {
	List<String> carList = Arrays.asList("Maruti", "Toyota", "Tesla");
	
//	Predicate<String> p = x -> x.contains("T");
//	for(String str: carList) {
//		if(p.test(str)) {
//			System.out.println(str);
//		}
//	}
	List<String> ans =carList.stream().filter(x -> x.contains("T")).collect(Collectors.toList());
	System.out.println(ans);
	
	//Intermediate 
	//terminal
	
	//forEach
	IntStream.range(1,6).forEach(System.out :: println);
	
    int countGreater10 = (int) Stream.of(10,20,30,40,50).filter(x->x>10).count();
    System.out.println(countGreater10);
    
    List<String> a =Arrays.asList("Java","Javascript", "Python" ,"Go").stream().filter(x-> x.contains("J")).collect(Collectors.toList());
    System.out.println(a);
    
    
    Stream.of("Java","Javascript", "Python" ,"Go").filter(x-> x.contains("J")).map(String :: toUpperCase).forEach(System.out :: println);
    
    List<Student> lst = new ArrayList<>();
    lst.add(new Student("Dileep", "Science", 90));
    lst.add(new Student("Dileep", "Maths", 80));
    lst.add(new Student("Rohith", "Science", 79));
    
    Map<String, List<String>> mp = new HashMap<>();
    
    lst.stream().forEach(stu -> {
    	mp.computeIfAbsent(stu.getName(), x->new ArrayList<>()).add(stu.getSubject());
    });
    System.out.println(mp);
    
    List<String> lst1 = Arrays.asList("A", "B");
    List<String> lst2 = Arrays.asList("c");
    System.out.println(
    		Stream.of(lst1, lst2).flatMap(List::stream).collect(Collectors.toList())
    		);
    
}
}
