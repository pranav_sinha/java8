package optional;

import java.util.Optional;

public class OptionalMain {

	
	//Optionals Are used for making null checks more efficient.
	
	// Best way to use otional is to create return type of client call as optional
	public static Optional<String> getName(){
		String name = "Pranav";
		String name1 = null;
		Optional<String> optional = Optional.ofNullable(name1);
		return optional;
	}
	public static void main(String[] args) {
		Optional<String> name = getName();
		System.out.println(name.orElse("Null String"));
	}

}
