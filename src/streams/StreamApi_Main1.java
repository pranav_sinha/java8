package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamApi_Main1 {
	public static void main(String[] args) {
		List<Integer> list1 = Arrays.asList(1,2,3,4,5);
		//print even integers
		List<Integer> evenList = list1.stream().filter(i->i%2==0).collect(Collectors.toList());
		System.out.println(evenList);
		
		List<Integer> squareList = list1.stream().map(i->i*i).collect(Collectors.toList());
		System.out.println(squareList);
		
		list1.stream().forEach(e->{
			System.out.println(e);
		});
		
		list1.stream().forEach(System.out::print);
	}
}  