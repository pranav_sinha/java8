package exception;

import java.io.FileNotFoundException;

//custom exceptions

class CustomException extends RuntimeException{
	CustomException(String msg){
		super(msg);   
	}
}
class A{
	void print() {
		try {
		throw new CustomException("custom ex handler");
		}catch(CustomException ex) {
		    throw new CustomException("custom ex handler");  //rethrowing of an exception
		}
	}
}
public class Excep {
public static void main(String [] args) {
	//int a = 20;
	int b = 4;
	
	try {
		A a = new A();
		a.print();
	}
	catch(CustomException e) {
		System.out.println("In main");
		System.out.println(e.getMessage());
	}
	finally {
		System.out.println("finally block executed");   //this block will always be executed
		//finally block is used to clean up the code
		//close the connection
	}
	
}
}
