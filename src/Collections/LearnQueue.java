package Collections;
import java.util.*;

import java.util.Comparator;

public class LearnQueue {
public static void main(String[]args) {
	Queue<Integer> pq = new PriorityQueue<>();  //min heap 
	Queue<Integer> maxpq = new PriorityQueue<>(Comparator.reverseOrder()); // max heap
	
	pq.offer(40);
	pq.offer(12);
	pq.offer(24);
	pq.offer(36);
	
	System.out.println(pq);
	pq.poll();
	System.out.println(pq);
	
	System.out.println(pq.peek());
	
	maxpq.offer(40);
	maxpq.offer(12);
	maxpq.offer(24);
	maxpq.offer(36);
	
	System.out.println(maxpq);
maxpq.poll();
	System.out.println(maxpq);
	
	System.out.println(maxpq.peek());
	
	
	ArrayDeque<Integer> adq = new ArrayDeque<>();
	
	adq.offer(23);
	adq.offer(12);
	adq.offer(45);
	adq.offer(26);
	
	System.out.println(adq);
	
	System.out.println(adq.peek());
	System.out.println(adq.peekFirst());
	System.out.println(adq.peekLast());
	
	System.out.println(adq.poll());
	
	System.out.println(adq.pollFirst());
	System.out.println(adq.pollLast());
	
	Queue<Integer> queue = new LinkedList<>();
	queue.offer(12);
	queue.offer(24);
	queue.offer(34);
	
	System.out.println(queue.poll());
	System.out.println(queue.peek());
}
}
