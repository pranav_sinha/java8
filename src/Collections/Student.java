package Collections;

public class Student {
	static int a = 50;  //instance variables   
	public static final String schoolName = "HIGH SCHOOL";  //constants
	//schoolName = "H";
	public static void print() {
//		int a = 10;
		System.out.println(a);
	}
public static void main(String[] args) {
     int a = 10;
     int ans =  a++;  //preincrement   -->
     System.out.println(ans);
     System.out.println(a);
     //a++;  //postincrement
     
     //shift Operators
     
     // Bitwise Operators
     
     int b = 5;                                                           //101
     int c = 6;                                                           //110
    int d = b & c;                                                      // 1 0 0   -->  4           1 1 1   
    System.out.println(d);                                                                          //2*2  2*1 2*0
                                                                             //0 1 1                     //4+2+1
    int e = b | c;
    System.out.println(e);
    
    
    int f = b ^ c;         
    System.out.println(f);
    
   int g = ~b;                                                   //2's Complement                             000000000000101
                                                                                                              //111111111111010       
                                                                                                           //2'sComplement = 1's Complement + 1;
                                                                                                           //1's Complement
                                                                                                             //100000000000000101
                                                                                                             //000000000000000001
                                                                                                             //100000000000000110    -6
   System.out.println(g);
}

}
     
     
//     //A & B       A B Output                    A NOT B      A B
//                   0*0  0                                     1 0
//}                  0*1  0                                     0 1
//
//}                  1*0  0
//                   1*1  1                        //A XOR B    A B Output
//                                                              0 0    0
//                                                              0 1    1
//                                                              1 0    1
//                                                              1 1    0
//     // A | B      0+0  0  
//                   0+1  1
//                   1+0  1
//                   1+1  1