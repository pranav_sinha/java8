package Collections;


class Test<T,U>{
	//an object of type T
	
	T obj1;
	U obj2;
	
	Test(T obj1, U obj2){
		this.obj1 = obj1;
		this.obj2= obj2;
	}
	
	public void print() {
		System.out.println(obj1);
		System.out.println(obj2);
	}
}
public class Generics {
public static void main(String[] args) {
//	Test<Integer> t = new Test<Integer>(123);
//	System.out.println(t.getObj());/
	
	Test<String, Integer> test = new Test<>("Hello", 24);
	test.print();
	
}
}
