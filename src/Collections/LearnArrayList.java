package Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class LearnArrayList {
public static void main(String [] args) {
	List<Integer> list = new ArrayList<>();   //type safety Generics are used
	list.add(10);
	list.add(20);
	list.add(30);
	list.add(40);
	list.add(50);
	
	List<Students> lst = new ArrayList<>();
	
	lst.add(new Students("AB", 2));
	lst.add(new Students("CD", 3));
	lst.add(new Students("FG", 1));
	
	Comparator<Students> cmpByName = Comparator.comparing(Students :: getName);	
	
	Comparator<Students> cmpByRoll = (Students o1, Students o2)  -> {return o1.getRollNo() - o2.getRollNo();};					
		
	
	Collections.sort(lst, cmpByName);
	
	System.out.println(lst);
	
	Collections.sort(lst, cmpByRoll);
	
	System.out.println(lst);
	
	 
	//System.out.println(list);
	
	//iterate the arraylist                        // internally how it works [10,20,30,40,50] - 10
//	for(int i=0;i<list.size();i++) {
//		System.out.println(list.get(i));
//	}
	
	//for each loop
	
//	for(Integer i: list) {
//		System.out.println(i);
//	}
	
	//with the help of iterators
	list.set(2, 1000);
	
	//list.clear();
	
	//list.remove(2);
	
	list.remove(Integer.valueOf(10));
	
	Iterator<Integer> it = list.iterator();
	
	while(it.hasNext()) {
		System.out.println(it.next());
	}
	
	
	
	
}
}
