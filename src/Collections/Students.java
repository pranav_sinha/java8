package Collections;

import java.util.Objects;

public class Students implements Comparable<Students>{
String name;
int rollNo;
public Students(String name, int rollNo) {
	super();
	this.name = name;
	this.rollNo = rollNo;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getRollNo() {
	return rollNo;
}
public void setRollNo(int rollNo) {
	this.rollNo = rollNo;
}
@Override
public String toString() {
	return "Students [name=" + name + ", rollNo=" + rollNo + "]";
}
@Override
public int hashCode() {
	return Objects.hash(rollNo);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Students other = (Students) obj;
	return rollNo == other.rollNo;
}
@Override
public int compareTo(Students o) {
	// TODO Auto-generated method stub
	// 0 -1 1
	return this.getRollNo() - o.getRollNo();
	//return this.getName().compareTo(o.getName());
}

}
