package Collections;

import java.util.List;
import java.util.Stack;

public class LearnStack {
public static void main(String[] args) {
	Stack<Integer> st = new Stack<>();
	st.push(7);
	st.push(8);
	st.push(9);
	
	while(!st.empty()) {
		int ans = st.peek();
	System.out.println(ans);
	st.pop();
	}
}
}
