package oops;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*Write a program to create class Shopper with two nested
classes named Wallet and Cart. Provide methods to add
the money in the wallet and add an item in the cart in
Shopper class and methods to view the balance in class
Wallet and delete an item in class Cart. Make sure that a
Shopper can have utmost one wallet and cart.
In main provide option to add item and add money and
when Shopper has added all the items then ask the user to
make payment. If sufficient balance is not available then
ask to add money.
[Take static array which has predefined items and their
prices in Shopper]*/

public class Shopper {
	private Wallet wallet;
	private Cart cart;
	
	static class Item{            //static
		String name;
		double price;
		
		Item(String name, double price){
			this.name = name;
			this.price = price;
		}
	}
	
	//we will instatntiate items Array
	
	static Item[] items  = {
	        new Item("item1",10.0),
	        new Item("item2",20.0),
	        new Item("item3",30.0),
	        new Item("item4",40.0),
	        new Item("item5",50.0)
	        
	};
			
	
	Shopper(){
		wallet = new Wallet();     //one one mapping
		cart = new Cart();   
	}
	class Wallet {
		private double balance;
		
		Wallet(){
			balance =0;
		}
		

		public void addMoney(double amount) {
			balance = balance + amount;
		}
		
		public void updateBalance(double amount) {
		    balance = amount;
		}

		public double getBalance() {
			return balance;
		}

	}

	class Cart {
      private Map<String, Integer> itemsInCart;
      
      Cart(){
    	  itemsInCart = new HashMap<>();
      }
      
      public void addItem(String itemName, int quantity) {
    	  boolean itemFound = false;
    	  for(Item item : items) {
    		  if(item.name.equals(itemName)) {
    			  itemFound = true;
    			  if(itemsInCart.containsKey(itemName)) {
    				  itemsInCart.put(itemName, itemsInCart.getOrDefault(item, 0)+ quantity);
    			  }else {
    				  itemsInCart.put(itemName, quantity);
    			  }
    			  
    		  }
    	  }
    	  
    	  if (itemFound == false) {
    		  System.out.println(itemName + "is not available in the store");
    	  }
      }
      
      public void deleteItem(String itemName, int quantity) {
          if(itemsInCart.containsKey(itemName)) {
        	  int currentQuantity = itemsInCart.get(itemName);
        	  if(currentQuantity >= quantity) {
        		  currentQuantity = currentQuantity - quantity;  //deletion part here  
        	  if(currentQuantity == 0) {
        		  itemsInCart.remove(itemName);
        	  }else {
        		  itemsInCart.put(itemName, currentQuantity);
        	  }
        
       }
        	  else {
        		  System.out.println("Quantity to delete exceeds the quantity in the cart");
        	  }
          }else {
        	  System.out.println(itemName+" not found in the cart");
          }
      }
	}
	
     public static void main(String[] args) {
    	 Shopper shopper = new Shopper();
    	 Scanner scanner = new Scanner(System.in);
    	 while(true) {
    		 System.out.println("Menu:");
    		 System.out.println("1. Add money to wallet");
    		 System.out.println("2. Add item to cart");
    		 System.out.println("3. View the balance");
    		 System.out.println("4. Delete Item from cart");
    		 System.out.println("5. Make the payment");
    		 System.out.println("6. Exit");
    		 
    		 System.out.println("Entere you choice: ");
    		 int choice = scanner.nextInt();
    		 
    		 switch(choice) {
    		 case 1: 
    			 System.out.println("Enter the amt to be added to the wallet: ");
    			 double amt = scanner.nextDouble();
    			 shopper.wallet.addMoney(amt);
    			 System.out.println("Added "+ amt + " to the wallet " );
    			 break;
    		 case 2: 
    			 System.out.println("Enter the item to be added to the cart: ");
    			 String itemName = scanner.next();
    			 System.out.println("Enter the quantity: ");
    			 int quantity = scanner.nextInt();
    			 shopper.cart.addItem(itemName, quantity);
    			 System.out.println(itemName + "Item added with quantity: " + quantity);
    			 break;
    		 case 3: 
    			 double balance = shopper.wallet.getBalance();
    			 System.out.println("Wallet balance: " + balance); 
    			 break;
    		 case 4: 
    			 System.out.println("Enter the item to be deleted from cart: ");
    			 String itemToDelete = scanner.next();
    			 System.out.println("Enter the quantity to delete: ");
    			 int quantityToDelete = scanner.nextInt();
    			 shopper.cart.deleteItem(itemToDelete, quantityToDelete);
    			 System.out.println("Deleted "+ quantityToDelete +  itemToDelete + " from cart " );
    			 break;
    			 
    		 case 5: 
    			 double totalAmount = 0;
    			 for(Map.Entry<String, Integer> entry: shopper.cart.itemsInCart.entrySet()) {
    				 String item = entry.getKey();
    				 int itemQuant = entry.getValue();
    				 
    				 for(Item itemObj: items) {
    					 if(itemObj.name.equals(item)) {
    						 totalAmount+= itemObj.price * itemQuant;
    					 }
    				 }
    			 }
    			 
    			 double walletBalance = shopper.wallet.getBalance();
    			 
    			 if(totalAmount > walletBalance) {
    				 System.out.println("Insufficient balance. please add more money to wallet");
    			 }else {
    				   walletBalance = walletBalance - totalAmount;
    				   shopper.wallet.updateBalance(walletBalance);
    				 System.out.println("Payment Successful");
    			 }
    			 break;
    			 
    		 case 6: 
    			 return;
    			 
    	     default:
    	    	 System.out.println("Invalid Choice. Please try again");
    		 }
    		 
    	 }
     }
}
