package oops;


class Shape{  //Base class
	String color;
}


class Triangle extends Shape{
	
}

class Circle extends Shape{
	
}


interface Animal{
	void run();
}

interface Human{
	//vsriables are public static final
	//methods are public and abstract
	//after java 8 you can create default as well as static methods
	void eat();
}

interface Tarzan extends Animal,Human{
	
}
class Hello implements Animal, Human{

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
}

public class OOPS {

}
