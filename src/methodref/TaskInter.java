package methodref;

@FunctionalInterface
public interface TaskInter {
	public void doTask();
}
