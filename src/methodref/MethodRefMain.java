package methodref;

public class MethodRefMain {

	public static void main(String[] args) {
	
		TaskInter task = ()->System.out.println("Lambda Direct Invodation to doTask method");
		task.doTask();
		
		//Referring the method of Stuff class in TaskInter doTask() method
		
		TaskInter task1 = Stuff::doStuff;
		task1.doTask();
		
		TaskInter task2 = Stuff::threadMethod;
		
		Runnable runnable = ()-> task2.doTask();
		
		Thread t = new Thread(runnable);
		
		t.start();
		
		Stuff s = new Stuff();
		
		Runnable runnable1 = s::printNumber;
		
		Thread t1 = new Thread(runnable1);
		
		t1.start();
	}
}
