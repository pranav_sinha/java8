package constructorref;

public class ConstructorRefMain {

	public static void main(String[] args) {
		Provider provider = ()->{
			return new Student();
		};
		
		provider.getStudent();
		
		
		// We can also do it by using constr. reference
		
		Provider provider1 = Student::new;
		provider1.getStudent();
	}

}
