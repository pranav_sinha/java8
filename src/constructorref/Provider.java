package constructorref;

@FunctionalInterface
public interface Provider {
 public Student getStudent();
}
