package jdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateJdbc {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		Connection con=ConnectionProvider.getConnection();
		
		String q = "update employee set name=? where emp_id=?;";
		
		PreparedStatement pstmt = con.prepareStatement(q);
		
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Name: ");
		
		String name = bf.readLine();
		
		System.out.println("Enter emp id: ");
		
		int id = Integer.parseInt(bf.readLine());
		
		pstmt.setString(1, name);
		
		pstmt.setInt(2, id);
		
		pstmt.executeUpdate();
		
	}

}
