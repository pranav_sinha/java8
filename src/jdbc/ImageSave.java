package jdbc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ImageSave {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		
		
		//Creating Connection
		Connection c = ConnectionProvider.getConnection();
		
		//Query to insert image
		String q = "insert into images(pic) values(?);";
		
		PreparedStatement pstmt=c.prepareStatement(q);
		
		// opem the fileChooser
		JFileChooser jfc = new JFileChooser();
		
		jfc.showOpenDialog(null);
		
		
		File file = jfc.getSelectedFile();
		
		//get the file
		FileInputStream f = new FileInputStream(file);
		
		//set the binary in preparedStatement
		pstmt.setBinaryStream(1, f, f.available());
		
		pstmt.executeUpdate();
		
		JOptionPane.showMessageDialog(null, null, "success", 0, null);
				

	}

}
