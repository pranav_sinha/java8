package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
	
	
	private static Connection con;
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		if(con ==  null) {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school","root","admin");
			return con;
		}
		else {
			return con;
		}
	}

}
