package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcMain1 {

	public static void main(String[] args) {
		
		//Step 1 Load the Driver
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Step 2 Get the connection
		
		try {
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/school","root","admin");
			
			if(con.isClosed())
			{
				System.out.println("Connection is Closed");
			}
			else {
				System.out.println("Connected to DB");
				
				// Step 3 Create Query and Perfomr Action  (Statement, PreparedStatement, CallableStatement)
				
				String create_table = "create table employee(emp_id int(20) primary key, name varchar(200) not null)";
				
				
				
				String q = "Select * from student;";
				
				Statement stmt = con.createStatement();
				
				stmt.executeUpdate(create_table);
				
				ResultSet rs = stmt.executeQuery(q);
				
				// Step 4 Traverse resultset to get the data row by row
				
				while(rs.next())
				{
					int roll_no = rs.getInt("roll_no");
					String name = rs.getString("name");
					
					System.out.println("Roll No: "+ roll_no +" Name: "+name);
				}
				
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
	}

}
