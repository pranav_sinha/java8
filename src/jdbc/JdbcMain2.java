package jdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JdbcMain2 {
   // insert dynamic values using preparedstatement
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/school","root","admin");
			
			String q = "insert into employee values(?,?)";
			
			PreparedStatement pstmt = con.prepareStatement(q);
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("Enter roll: ");
			int roll_no = Integer.parseInt(bufferedReader.readLine());
			System.out.println("Enter name: ");
			String name = bufferedReader.readLine();
			pstmt.setInt(1, roll_no);
			pstmt.setString(2, name);
			
			pstmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
